// saga effects
import { all } from "redux-saga/effects";

// saga
import {
    watchGetRuleSaga,
    watchUpdateRuleSaga,
    watchRemoveRuleSaga,
} from "./RuleSaga";

export default function* rootSaga() {
    yield all([
        watchGetRuleSaga(),
        watchUpdateRuleSaga(),
        watchRemoveRuleSaga(),
    ]);
}
