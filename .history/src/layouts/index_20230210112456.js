import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
} from "@ant-design/icons";
import { Breadcrumb, Layout, Menu, theme } from "antd";
import React, { useState } from "react";
import "./styles.css";
import menuItems from "../constants/itemsMenu";
import headerItems from "../constants/itemsHeader";
import { ADMINSTRATIONS, DASHBOARD, RULES, SERVICES, STATISTICS } from "../constants/itemsContants";
import { useNavigate } from "react-router-dom";
import Router from "../router";

const { Header, Content, Sider } = Layout;

const LayoutWrapper = () => {
  const [collapsed, setCollapsed] = useState(false);
  const navigate = useNavigate();
  const {
    token: { colorBgContainer },
  } = theme.useToken();

  const onClick = (e) => {
    navigate(e.key);
  }
  return (
      <Layout>
        <Header className="header">
          <div className="logo" />
          <Menu
            theme="dark"
            mode="horizontal"
            defaultSelectedKeys={["2"]}
            items={headerItems}
          />
          {React.createElement(
            collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
            {
              className: "trigger",
              onClick: () => setCollapsed(!collapsed),
            }
          )}
        </Header>
        <Layout>
          <Sider
            width={200}
            style={{
              background: colorBgContainer,
              overflow: 'auto',
              height: '100vh',
            }}
            trigger={null}
            collapsible
            collapsed={collapsed}
            
          >
            <Menu
              mode="inline"
              defaultSelectedKeys={[DASHBOARD]}
              defaultOpenKeys={[STATISTICS,SERVICES,RULES,ADMINSTRATIONS]}
              style={{
                height: "100%",
                borderRight: 0,
              }}
              items={menuItems}
              onClick={(e) => onClick(e)}
            />
          </Sider>
          <Layout
            style={{
              padding: "0 24px 24px",
            }}
          >
            <Breadcrumb
              style={{
                margin: "16px 0",
              }}
            >
              <Breadcrumb.Item>BreadCrumb</Breadcrumb.Item>
              <Breadcrumb.Item>BreadCrumb</Breadcrumb.Item>
              <Breadcrumb.Item>BreadCrumb</Breadcrumb.Item>
            </Breadcrumb>
            <Content
              style={{
                padding: 24,
                margin: 0,
                minHeight: "85vh",
                background: colorBgContainer,
              }}
            >
              <Router></Router>
            </Content>
          </Layout>
        </Layout>
      </Layout>
  );
  // hello
};
export default LayoutWrapper;
