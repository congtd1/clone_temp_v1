import React from "react";
import { useDispatch, useSelector } from "react-redux";
import TypeAction from "../constants/TypeAction";
import { selectRules } from "../redux/slice/RuleSlice";

function RuleTest() {
  const dispatch = useDispatch();

  const testClick = () => {
    dispatch({
      type: TypeAction.RULE.GET_RULE,
      payload: { numberPage: 1, numberOnePage: 5 },
    });
  };

  return (
    <div>
      <button role="presentation" onClick={testClick} />
    </div>
  );
}

export default RuleTest;
