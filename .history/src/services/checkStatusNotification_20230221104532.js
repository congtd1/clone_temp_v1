import ok from "../assets/icons/icons8-ok-96.png"
import warning from "../assets/icons/icons8-general-warning-sign-96.png";
import error from "../assets/icons/icons8-high-priority-94.png";
import styles from "../screen/pages/Dashboard/Service/Censor/Censor.module.scss";

const checkStatusNotification = (value) => {
    if (value === "ok") {
      return (
        <h4 style={{ color: "#3bff3b" }} className={styles.status}>
          Hoạt động ổn định
        </h4>
      );
    }
    if (value === "warning") {
      return (
        <h4 style={{ color: "#f1ea15" }} className={styles.status}>
          Đang gặp vấn đề
        </h4>
      );
    }
    if (value === "error") {
      return (
        <h4 style={{ color: "red" }} className={styles.status}>
          Gặp vấn đề nghiêm trọng
        </h4>
      );
    }
  };

  export default checkStatusNotification;
