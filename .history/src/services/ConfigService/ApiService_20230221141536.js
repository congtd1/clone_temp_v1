import axios from "axios";

const ApiService = (configuration = {}) => {
  let baseURL = "http://192.168.14.60:8991/";
  let headers = {
    "Content-Type": "application/json",
  };

  const axiosInstance = axios.create({
    baseURL,
    timeout: 10000,
    headers,
    ...configuration,
  });

  const tokenUser = localStorage.getItem("tokenUser");
  console.log(tokenUser);

  axiosInstance.interceptors.request.use(
    (config) => {
      if (tokenUser) {
        config.headers.Authorization = `Bearer ${tokenUser}`;
      }
      return config;
    },
    (error) => Promise.reject(error)
  );

  axiosInstance.interceptors.response.use(
    (response) => response,
    (error) => {
      if (error?.response?.status === 401) {
        console.log("error 401 -> token expired");
      }

      if (error?.response?.status === 503) {
        console.log("Something went wrong. Please try later!");
      }

      return Promise.reject(error);
    }
  );
  
  return axiosInstance;
};

export default ApiService;
