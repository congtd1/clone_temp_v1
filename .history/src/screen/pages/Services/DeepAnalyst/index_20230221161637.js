import React from "react";

import { Tabs } from "antd";

import ContentTab1 from "./component/Tab/ContentTab1";
import ContentTab2 from "./component/Tab/ContentTab2";
import DisplayDALeft from "./component/Tab/DisplayDALeft";
import DisplayDARight from "./component/Tab/DisplayDARight";
import BtnaddDA from "./component/Button/BtnaddDA";

const onChange = (key) => {
  console.log(key);
};

const items = [
  {
    key: "1",
    label: (
      <div>
        <DisplayDALeft />
      </div>
    ),
    children: (
      <>
        <ContentTab1 />
      </>
    ),
  },
  {
    key: "2",
    label: (
      <div>
        <DisplayDARight />
      </div>
    ),
    children: (
      <>
        <ContentTab2 />
      </>
    ),
  },
];

const DeepAnalyst = () => {
  return (
    <div>
      <div>
        <BtnaddDA />
      </div>
      <Tabs
        // style={{ marginLeft: "2px" }}
        defaultActiveKey="1"
        items={items}
        onChange={onChange}
      />
    </div>
  );
};

export default DeepAnalyst;
