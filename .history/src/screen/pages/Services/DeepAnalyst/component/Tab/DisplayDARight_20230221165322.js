import React from "react";
import BtnRight from "../Button/BtnRight";
import { Col, Row } from "antd";
import styled from "../../../sass/DisplayTopRight.module.scss";

const DisplayDARight = () => {
  return (
    <div className={styled.container}>
      <div className={styled.container_left}>
        <BtnRight />
      </div>
      
      <div className={styled.container_content}>
        <div className={styled.container_content_text}>
          <h2>CPU:</h2>
          <span>16core</span>
        </div>
        <div className={styled.container_content_text}>
          <h2>RAM:</h2>
          <span>32GB</span>
        </div>
        <div className={styled.container_content_text}>
          <h2>Total Disk:</h2>
          <span>1TB</span>
        </div>
        <div className={styled.container_content_text}>
          <h2>Log disk:</h2>
          <span>400GB</span>
        </div>
        <div className={styled.container_content_text}>
          <h2>Traffic:</h2>
          <span>125Mbps</span>
        </div>
        <div className={styled.container_content_text}>
          <h2>Trạng thái:</h2>
          <span>Không hoạt động</span>
        </div>
      </div>
    </div>
  );
};

export default DisplayDARight;
