import React from "react";
import { Input } from "antd";
import { Col, Row } from "antd";
import styled from "../../sass/ContentTab1.module.scss";

const { TextArea } = Input;
const style = {
  background: "#fff",
  padding: "8px 0",
};

const ContentTab2 = () => {
  return (
    <div className={styled.container}>
      <div className={styled.container_note}>
        <h2>Ghi chú</h2>
        <TextArea rows={4} />
      </div>
      <div className={styled.container_parameter}>
        <h2>Metrics (Average)</h2>
        <div className={styled.container_parameter_display}>
          <Row gutter={[16, 24]}>
            <Col className="gutter-row" span={6}>
              <div style={{ style, border: "1px solid #000" }}>
                <h2>Live hosts</h2>
                <span className={styled.text}>4/6</span>
              </div>
            </Col>

            <Col className="gutter-row" span={6}>
              <div style={{ style, border: "1px solid #000" }}>
                <h2>CPU Usage</h2>
                <span className={styled.text}>70.5%</span>
              </div>
            </Col>

            <Col className="gutter-row" span={6}>
              <div style={{ style, border: "1px solid #000" }}>
                <h2>Memory Usage</h2>
                <span className={styled.text}>88.9%</span>
              </div>
            </Col>

            <Col className="gutter-row" span={6}>
              <div style={{ style, border: "1px solid #000" }}>
                <h2>Logs Disk Usage</h2>
                <span className={styled.text}>25.5%</span>
              </div>
            </Col>

            <Col className="gutter-row" span={6}>
              <div style={{ style, border: "1px solid #000" }}>
                <h2>Loaded Rules</h2>
                <span className={styled.text}>4863</span>
              </div>
            </Col>

            <Col className="gutter-row" span={6}>
              <div style={{ style, border: "1px solid #000" }}>
                <h2>Failed Rules</h2>
                <span className={styled.text}>2</span>
              </div>
            </Col>

            <Col className="gutter-row" span={6}>
              <div style={{ style, border: "1px solid #000" }}>
                <h2>Traffic Volume</h2>
                <span className={styled.text}>8.24Kb/s</span>
              </div>
            </Col>

            <Col className="gutter-row" span={6}>
              <div style={{ style, border: "1px solid #000" }}>
                <h2>Kernel Drops</h2>
                <span className={styled.text}>0,0%</span>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </div>
  );
};

export default ContentTab2;
