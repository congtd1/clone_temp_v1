import { Route, Routes } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import DataManage from "./pages/DataManage";
import {
  CENSOR,
  DASHBOARD,
  DATAMANAGE,
  RULEMANAGE,
  RULESSETTING,
} from "../constants/itemsContants";
import RuleManage from "./pages/Rule/RuleManage";
import RuleConfig from "./pages/Rule/RuleConfig";
import Censor from "./pages/Services/Censor";

const Router = () => {
  return (
    <Routes>
      <Route path={DASHBOARD} element={<Dashboard></Dashboard>} />
      <Route path={DATAMANAGE} element={<DataManage></DataManage>} />
      <Route path={RULEMANAGE} element={<RuleManage></RuleManage>} />
      <Route path={RULESSETTING} element={<Censor></Censor>} />
      <Route path={CENSOR} element={<RuleConfig></RuleConfig>} />
    </Routes>
  );
};

export default Router;
