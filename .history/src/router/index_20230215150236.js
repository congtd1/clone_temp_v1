import { Navigate, Route, Routes, useLocation } from "react-router-dom";

import {
  CENSOR,
  CGATE,
  DASHBOARD,
  DATAMANAGE,
  DEEPANALYST,
  RULEMANAGE,
  RULESSETTING,
  SIGNIN,
} from "../constants/itemsContants";
import { Suspense, lazy, useEffect } from "react";
import * as React from "react";
import PageLoader from "../components/PageComponents/PageLoader";
import DeepAnalyst from "./pages/Services/DeepAnalyst";
import Cgate from "./pages/Services/Cgate";

const Timeout = 300;
const Dashboard = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/Dashboard")), Timeout);
  });
});
const DataManage = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/DataManage")), Timeout);
  });
});
const RuleSetting = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/Rule/RuleConfig")), Timeout);
  });
});
const RuleManage = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/Rule/RuleManage")), Timeout);
  });
});
const Signin = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/Signin")), Timeout);
  });
});
const Censor = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(() => resolve(import("./pages/Services/Censor")), Timeout);
  });
});

const Router = () => {
  const location = useLocation();
  const pathSignIn = `/${SIGNIN}`;
  const Redirect = async () => {
    if (
      pathSignIn.includes(location.pathname) ||
      location.pathname === "/" ||
      location.pathname === " "
    ) {
      return <Navigate to={SIGNIN} />;
    }
  };
  useEffect(() => {
    Redirect();
  }, []);
  return (
    <Routes>
      <Route
        path={DASHBOARD}
        element={
          <Suspense fallback={<PageLoader />}>
            <Dashboard />
          </Suspense>
        }
      />
      <Route
        path={DATAMANAGE}
        element={
          <Suspense fallback={<PageLoader />}>
            <DataManage />
          </Suspense>
        }
      />
      <Route
        path={RULEMANAGE}
        element={
          <Suspense fallback={<PageLoader />}>
            <RuleManage />
          </Suspense>
        }
      />
      <Route
        path={RULESSETTING}
        element={
          <Suspense fallback={<PageLoader />}>
            <RuleSetting />
          </Suspense>
        }
      />
      <Route
        path={SIGNIN}
        element={
          <Suspense fallback={<PageLoader />}>
            <Signin />
          </Suspense>
        }
      />
      <Route
        path={CENSOR}
        element={
          <Suspense fallback={<PageLoader />}>
            <Censor />
          </Suspense>
        }
      />
      <Route path={"/test"} element={<PageLoader />} />
      <Route path={CGATE} element={<Cgate></Cgate>} />
      <Route path={DEEPANALYST} element={<DeepAnalyst></DeepAnalyst>} />
    </Routes>
  );
};

export default Router;
