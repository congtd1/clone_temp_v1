import { Navigate, Route, Routes, useLocation } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import DataManage from "./pages/DataManage";
import {
  CENSOR,
  CGATE,
  DASHBOARD,
  DATAMANAGE,
  RULEMANAGE,
  RULESSETTING,
  SIGNIN,
} from "../constants/itemsContants";
import RuleConfig from "./pages/Rule/RuleConfig";
import SignIn from "./pages/Signin";
import { useEffect } from "react";
import RuleManage from "./pages/Rule/RuleManage";
import Censor from "./pages/Services/Censor";
import Cgate from "./pages/Dashboard/Service/Cgate";

const Router = () => {
  const location = useLocation();
  const pathSignIn = `/${SIGNIN}`;
  const Redirect = async () => {
    if (
      pathSignIn.includes(location.pathname) ||
      location.pathname === "/" ||
      location.pathname === " "
    ) {
      return <Navigate to={SIGNIN} />;
    }
  };
  useEffect(() => {
    Redirect();
  }, []);
  return (
    <Routes>
      <Route path={DASHBOARD} element={<Dashboard></Dashboard>} />
      <Route path={DATAMANAGE} element={<DataManage></DataManage>} />
      <Route path={RULEMANAGE} element={<RuleManage></RuleManage>} />
      <Route path={RULESSETTING} element={<RuleConfig></RuleConfig>} />
      <Route path={SIGNIN} element={<SignIn></SignIn>}></Route>
      <Route path={CENSOR} element={<Censor></Censor>} />
      <Route path={CGATE} element={<Cgate></Cgate>} />
    </Routes>
  );
};

export default Router;
