import { Route, Routes } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import DataManage from "./pages/DataManage";
import {
  DASHBOARD,
  DATAMANAGE,
  RULEMANAGE,
  RULESSETTING,
  CENSOR,
  CGATE,
  DEEPANALYST
} from "../constants/itemsContants";
import RuleManage from "./pages/Rule/RuleManage";
import RuleConfig from "./pages/Rule/RuleConfig";
import Censor from "./pages/Services/Censor";
import DeepAnalyst from "./pages/Services/DeepAnalyst";
import Cgate from "./pages/Services/Cgate";

const Router = () => {
  return (
    <Routes>
      <Route path={DASHBOARD} element={<Dashboard></Dashboard>} />
      <Route path={DATAMANAGE} element={<DataManage></DataManage>} />
      <Route path={RULEMANAGE} element={<RuleManage></RuleManage>} />
      <Route path={RULESSETTING} element={<RuleConfig></RuleConfig>} />
      <Route path={CENSOR} element={<Censor></Censor>} />
      <Route path={CGATE} element={<Cgate></Cgate>} />
      <Route path={DEEPANALYST} element={<DeepAnalyst></DeepAnalyst>} />

    </Routes>
  );
};

export default Router;
