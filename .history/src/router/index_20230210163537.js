import { Route, Routes } from "react-router-dom";
import Dashboard from "./pages/Dashboard";
import DataManage from "./pages/DataManage";
import { DASHBOARD, DATAMANAGE, RULEMANAGE } from "../constants/itemsContants";
import RuleManage from "./pages/Rule/RuleManage";

const Router = () => {
  return (
    <Routes>
      <Route path={DASHBOARD} element={<Dashboard></Dashboard>} />
      <Route path={DATAMANAGE} element={<DataManage></DataManage>} />
      <Route path={RULEMANAGE} element={<RuleManage></RuleManage>} />
      <Route path={RULEMANAGE} element={<RuleManage></RuleManage>} />
    </Routes>
  );
};

export default Router;
