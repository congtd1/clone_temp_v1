import React from "react";

import { Tabs } from "antd";

import ContentTab1 from "./component/Tab/ContentTab1";
import ContentTab2 from "./component/Tab/ContentTab2";
import BtnaddCgate from './component/Button/BtnaddCgate'
import DisplayCgateLeft from './component/Tab/DisplayCgateLeft'
import DisplayCgateRight from './component/Tab/DisplayCgateLeft'

 
const onChange = (key) => {
  console.log(key);
};

const items = [
  {
    key: "1",
    label: (
      <div style={{ marginLeft: "100px" }}>
        <DisplayCgateLeft />
      </div>
    ),
    children: (
      <>
        <ContentTab1 />
      </>
    ),
  },
  {
    key: "2",
    label: (
      <div style={{ marginLeft: "100px" }}>
        <DisplayCensorRight />
      </div>
    ),
    children: (
      <>
        <ContentTab2 />
      </>
    ),
  },
];

const Cgate = () => {
  return (
    <div>
      <div>
        <BtnaddCgate />
      </div>
      <Tabs
        style={{ marginLeft: "2px" }}
        defaultActiveKey="1"
        items={items}
        onChange={onChange}
      />
    </div>
  );
};

export default Cgate;
