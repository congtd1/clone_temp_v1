import React from "react";
import { Col, Row } from "antd";
import BtnLeft from "../Button/BtnLeft";
import styled from "../sass/DisplayCgateLeft.module.scss";

const DisplayCgateLeft = () => {
  return (
    <div className={styled.container}>
      <Row>
        <Col span={8}>
          <BtnLeft />
        </Col>
        <Col span={8}>
          <div className={styled.container_content}>
            <div className={styled.container_content_text}>
              <h2>CPU:</h2>
              <span>16core</span>
            </div>
            <div className={styled.container_content_text}>
              <h2>RAM:</h2>
              <span>32GB</span>
            </div>
            <div className={styled.container_content_text}>
              <h2>Total Disk:</h2>
              <span>1TB</span>
            </div>
            <div className={styled.container_content_text}>
              <h2>Log disk:</h2>
              <span>400GB</span>
            </div>
            <div className={styled.container_content_text}>
              <h2>Traffic:</h2>
              <span>125Mbps</span>
            </div>
            <div className={styled.container_content_text}>
              <h2>Trạng thái:</h2>
              <span>Đang hoạt động</span>
            </div>
          </div>
        </Col>

        <Col span={8}>
          <div className={styled.container_right}>
            <div className={styled.container_right_wrapper}>
              <h2>Phiên bản Rules đang sử dụng:</h2>
              <span style={{ color: "#000", fontWeight: "300" }}>
                1.00.30.12.2022.00a
              </span>
            </div>

            <div className={styled.container_right_wrapper}>
              <h2>Phiên bản Rules đang chờ cập nhật:</h2>
              <span style={{ color: "#000", fontWeight: "300" }}>
                1.00.30.12.2022.00a
              </span>
            </div>

            <h3
              style={{ marginTop: "26px", color: "#2EB100", fontWeight: "300" }}
            >
              Đang sử dụng phiên bản mới nhất
            </h3>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default DisplayCgateLeft;
