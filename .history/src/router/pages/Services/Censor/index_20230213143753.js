import React from "react";

import { Tabs } from "antd";
import DisplayCensorLeft from "./component/Tab/DisplayCensorLeft";
import DisplayCensorRight from "./component/Tab/DisplayCensorRight";
import ContentTab1 from "./component/ContentTab1";
import ContentTab2 from "./component/ContentTab2";

const onChange = (key) => {
  console.log(key);
};

const items = [
  {
    key: "1",
    label: (
      <div>
        <DisplayCensorLeft />
      </div>
    ),
    children: (
      <>
        <ContentTab1 />
      </>
    ),
  },
  {
    key: "2",
    label: (
      <div>
        <DisplayCensorRight />
      </div>
    ),
    children: (
      <>
        <ContentTab2 />
      </>
    ),
  },
];

const Censor = () => {
  return (
    <div>
      <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
    </div>
  );
};

export default Censor;
