import React from "react";

import { Tabs } from "antd";

import DisplayCensorLeft from "./component/Tab/DisplayCensorLeft";
import DisplayCensorRight from "./component/Tab/DisplayConsorRight";
import ContentTab1 from "./component/Tab/ContentTab1";
import ContentTab2 from "./component/Tab/ContentTab2";

import BtnaddCensor from "./component/Button/BtnaddCensor";

const onChange = (key) => {
  console.log(key);
};

const items = [
  {
    key: "1",
    label: (
      <div style={{ marginLeft: "100px" }}>
        <DisplayCensorLeft />
      </div>
    ),
    children: (
      <>
        <ContentTab1 />
      </>
    ),
  },
  {
    key: "2",
    label: (
      <div style={{ marginLeft: "100px" }}>
        <DisplayCensorRight />
      </div>
    ),
    children: (
      <>
        <ContentTab2 />
      </>
    ),
  },
];

const Censor = () => {
  return (
    <div>
      <div>
        <BtnaddCensor />
      </div>
      <Tabs
        style={{ marginLeft: "2px" }}
        defaultActiveKey="1"
        items={items}
        onChange={onChange}
      />
    </div>
  );
};

export default Censor;
