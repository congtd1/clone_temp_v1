import React from "react";
import BtnRight from "../Button/BtnRight";
import { Col, Row } from "antd";
import styled from "../sass/DisplayCensorRight.module.scss";

const DisplayCensorRight = () => {
  return (
    <div className={styled.container}>
      <Row>
        <Col span={8}>
          <BtnRight />
        </Col>
        <Col span={8}>
          <div>
            <div>
              <h2>CPU:</h2>
              <span>16core</span>
            </div>
            <div>
              <h2>RAM:</h2>
              <span>32GB</span>
            </div>
            <div>
              <h2>Total Disk:</h2>
              <span>1TB</span>
            </div>
            <div>
              <h2>Log disk:</h2>
              <span>400GB</span>
            </div>
            <div>
              <h2>Traffic:</h2>
              <span>125Mbps</span>
            </div>
            <div>
              <h2>Trạng thái:</h2>
              <span>Đang hoạt động</span>
            </div>
          </div>
        </Col>
        <Col span={8}>
          <div>
            <div>
              <h2>Phiên bản Rules đang sử dụng:</h2>
              <span>1.00.30.12.2022.00a</span>
            </div>

            <div>
              <h2>Phiên bản Rules đang chờ cập nhật:</h2>
              <span>1.00.30.12.2022.00a</span>
            </div>

            <div>
              <span>Đang sử dụng phiên bản mới nhất</span>
            </div>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default DisplayCensorRight;
