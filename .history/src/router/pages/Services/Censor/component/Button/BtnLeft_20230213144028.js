import React from 'react';
import { PoweroffOutlined, RedoOutlined } from '@ant-design/icons';
import svg1 from '../../../../../../assets/images/iconsMonitor.svg';
import svg2 from '../../../../../../assets/images/iconsChange.svg';

import styled from './BtnLeft.module.scss';

const BtnLeft = () => {
  return (
    <div className={styled.container}>
      <div className={styled.container_monitor}>
        <img src={svg1} alt="" />
      </div>
      <span style={{ marginLeft: '26px', color: '#000', marginBottom: '10px' }}>Censor1</span>
      <div className={styled.container_btn}>
        <PoweroffOutlined className={styled.container_btn_icon} />
        <RedoOutlined className={styled.container_btn_icon} />
        <img src={svg2} className={styled.container_btn_icon} alt="" />
      </div>
    </div>
  );
};

export default BtnLeft;
