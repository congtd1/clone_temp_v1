import React from "react";
import { PoweroffOutlined, RedoOutlined } from "@ant-design/icons";
import svg1 from "../../../../../../assets/iconsMonitor.svg";
import svg2 from "../../../../../../assets/iconsChange.svg";

const BtnRight = () => {
  return (
    <div>
      <div>
        {/* <img src={svg1} alt="" /> */}
      </div>
      <span>Consor2</span>
      <div>
        <PoweroffOutlined />
        <RedoOutlined />
        {/* <img src={svg2} alt="" /> */}
      </div>
    </div>
  );
};

export default BtnRight;
