import React from "react";
import { Button, Modal } from "antd";
import { useState } from "react";
import { Input } from "antd";

const BtnaddCensor = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <Button style={{ background: "#D9D9D9B2" }} onClick={showModal}>
        Thêm máy chủ Censor
      </Button>
      <Modal
        title="Tạo máy chủ Censor"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <div>
          <span>Tên máy chủ *:</span>
          <Input placeholder="Basic usage" />
          
        </div>
      </Modal>
    </>
  );
};

export default BtnaddCensor;
