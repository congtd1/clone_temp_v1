import React from 'react';
import { Button, Input, Table } from 'antd';
import { useState } from 'react';
import { ProfileOutlined, UpOutlined } from '@ant-design/icons';
import svg1 from '../../../../assets/images/svg1.svg';
import svg2 from '../../../../assets/images/svg2.svg';
import svg3 from '../../../../assets/images/svg3.svg';
import svg4 from '../../../../assets/images/svg4.svg';
import svg5 from '../../../../assets/images/svg5.svg';
import svg7 from '../../../../assets/images/svg7.svg';
import BtnCategory from '../component/Button/BtnCategory';
import BtnSearch from '../component/Button/BtnSearch';
import BtnStatus from '../component/Button/BtnStatus';
import BtnTag from '../component/Button/BtnTag';
import Dropdowns from '../component/Model/Dropdowns';
import ModelBtnadd from '../component/Model/ModelBtnadd';
import ModelBtnimport from './ModelBtnimport';

const { Search } = Input;

const columns = [
  {
    title: <div style={{ textAlign: 'center' }}>ID</div>,
    dataIndex: 'id',
  },
  {
    title: (
      <div style={{ textAlign: 'center' }}>
        <ProfileOutlined style={{ fontSize: '22px', marginTop: '4px', marginRight: '6px' }} />
        SID:rev
      </div>
    ),
    dataIndex: 'rev',
  },
  {
    title: <div style={{ textAlign: 'center' }}>Mô tả</div>,
    dataIndex: 'descripttion',
  },
  {
    title: (
      <div style={{ textAlign: 'center' }}>
        <BtnCategory />
      </div>
    ),
    dataIndex: 'category',
  },
  {
    title: (
      <div style={{ textAlign: 'center' }}>
        <BtnStatus />
      </div>
    ),
    dataIndex: 'status',
  },
  {
    title: (
      <div style={{ textAlign: 'center' }}>
        <BtnTag />
      </div>
    ),
    dataIndex: 'tags',
  },
  {
    title: <div style={{ textAlign: 'center' }}>Chức năng</div>,
    dataIndex: 'func',
  },
];

const data = [];
for (let i = 0; i < 46; i++) {
  data.push({
    key: i,
    id: <div style={{ textAlign: 'center' }}>80031</div>,
    rev: <div style={{ textAlign: 'center' }}>900312543:1</div>,
    descripttion: `SSLBL: Malicious SSL certificate detected`,
    category: <div style={{ textAlign: 'center' }}>SSLBL</div>,
    status: (
      <div style={{ textAlign: 'center' }}>
        <Button
          type="default"
          style={{ borderRadius: '6px', backgroundColor: '#2EB100', color: '#fff', fontWeight: 700, border: 'none' }}>
          Duyệt
        </Button>
        {/* <Button type="default" style={{ borderRadius: '6px', backgroundColor: '#B4B3B3', color: '#fff', fontWeight: 700, border: 'none' }}>
          Quá hạn
        </Button>
        <Button type="default" style={{ borderRadius: '6px', backgroundColor: '#DE2020', color: '#fff', fontWeight: 700, border: 'none' }}>
          Hủy
        </Button>
        <Button type="default" style={{ borderRadius: '6px', backgroundColor: '#C9CC1F', color: '#fff', fontWeight: 700, border: 'none' }}>
          Xem lại
        </Button> */}
      </div>
    ),
    tags: (
      <div style={{ textAlign: 'center' }}>
        <Button
          type="default"
          style={{
            borderRadius: '6px',
            backgroundColor: '#3FADDC',
            color: '#000',
            fontWeight: 400,
            marginRight: '10px',
            border: 'none',
          }}>
          shellcode-detect
        </Button>
        {/* 
        <Button type="default" style={{ borderRadius: '6px', backgroundColor: '#3FADDC', color: '#000', fontWeight: 400, marginRight: '10px', border: 'none' }}>
          dns
        </Button>

        <Button type="default" style={{ borderRadius: '6px', backgroundColor: '#3FADDC', color: '#000', fontWeight: 400, marginRight: '10px', border: 'none' }}>
          ftp
        </Button>

        <Button type="default" style={{ borderRadius: '6px', backgroundColor: '#3FADDC', color: '#000', fontWeight: 400, marginRight: '10px', border: 'none' }}>
          aleft
        </Button> */}
      </div>
    ),
    address: `London, Park Lane no. ${i}`,
    func: (
      <div style={{ textAlign: 'center' }}>
        <img style={{ marginLeft: '6px' }} src={svg3}></img>
        <img style={{ marginLeft: '6px' }} src={svg2}></img>
        <img style={{ marginLeft: '6px' }} src={svg5}></img>
        <img style={{ marginLeft: '6px' }} src={svg4}></img>

        <img style={{ marginLeft: '6px' }} src={svg1}></img>
      </div>
    ),
  });
}
const App = () => {
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [loading, setLoading] = useState(false);
  const start = () => {
    setLoading(true);
    // ajax request after empty completing
    setTimeout(() => {
      setSelectedRowKeys([]);
      setLoading(false);
    }, 1000);
  };
  const onSelectChange = newSelectedRowKeys => {
    console.log('selectedRowKeys changed: ', newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };
  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };
  const hasSelected = selectedRowKeys.length > 0;
  return (
    <div>
      <div
        style={{
          marginBottom: 16,
        }}>
        <div style={{ display: 'flex' }}>
          <Dropdowns />
          <BtnSearch />

          <div style={{ marginLeft: 'auto', marginRight: '10px' }}>
            {/* <Space wrap>
              <Button type="default" style={{ backgroundColor: '#D9D9D9', border: '1px solid #ccc' }}>
                Tạo mới Rules
              </Button>
            </Space> */}

            <ModelBtnadd />

            <ModelBtnimport />
          </div>
        </div>

        <span
          style={{
            marginLeft: 8,
          }}>
          {hasSelected ? `Selected ${selectedRowKeys.length} items` : ''}
        </span>
      </div>
      <div style={{ position: 'relative' }}>
        <Table
          pagination={{
            position: ['bottomCenter'],
            pageSize: 10,
          }}
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          bordered
        />
        <div style={{ position: 'absolute', bottom: '16px', left: '30px' }}>
          Số bản ghi trong trang{' '}
          <Button type="default" style={{ background: '#D9D9D9', border: '1px solid #ccc', marginLeft: '20px' }}>
            15
            <UpOutlined />
          </Button>
        </div>
        <div style={{ position: 'absolute', bottom: '25px', right: '30px' }}>
          Tổng số{' '}
          <a href="" style={{ fontWeight: '700' }}>
            145
          </a>{' '}
          kết quả trong{' '}
          <a href="" style={{ fontWeight: '700' }}>
            15
          </a>{' '}
          trang
        </div>
      </div>
    </div>
  );
};
export default App;
