import React from "react";
import { Button, Space } from "antd";
import styled from "../sass/BtnSave.module.scss";

const BtnSave = () => {
  return (
    <div>
      <Button className={styled.btnSave} type="primary">
        Lưu
      </Button>
    </div>
  );
};

export default BtnSave;
