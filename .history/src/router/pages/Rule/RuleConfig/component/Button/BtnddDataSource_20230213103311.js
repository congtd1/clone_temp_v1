import React from 'react';
import { Button, Modal } from 'antd';
import { useState } from 'react';

const BtnAdddataSource = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <Button
        type="default"
        style={{ background: '#D9D9D9B2', border: '1px solid #000', marginRight: '31px' }}
        onClick={showModal}>
        Thêm nguồn cấp dữ liệu
      </Button>
      <Modal title="Basic Modal" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <p>Add data...</p>
      </Modal>
    </>
  );
};

export default BtnAdddataSource;
