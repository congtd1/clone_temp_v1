import React from 'react';
import { Button, Space } from 'antd';

const BtnSave = () => {
  return (
    <div>
      <Button
        className={styled.BtnSave}
        type="primary">
        Lưu
      </Button>
    </div>
  );
};

export default BtnSave;
