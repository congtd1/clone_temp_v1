import React from 'react';
import { Button, Space } from 'antd';

const BtnSave = () => {
  return (
    <div>
      <Button
        style={{ marginLeft: '140px', marginTop: '8px', background: '#1494CB', width: '90px', height: '30px' }}
        type="primary">
        Lưu
      </Button>
    </div>
  );
};

export default BtnSave;
