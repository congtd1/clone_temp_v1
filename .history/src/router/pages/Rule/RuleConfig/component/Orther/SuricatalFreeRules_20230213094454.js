import React from 'react';
import styled from '../sass/SuricatalFreeRules.module.scss';

import { CheckOutlined } from '@ant-design/icons';
import { Space, Switch } from 'antd';

import { Tabs } from 'antd';
import FuncConnect from '../Func/FuncConnect';
import BtnSave from '../Button/BtnSave';
import FuncConfig from '../Func/FuncConnect';
const onChange = key => {
  console.log(key);
};

const items = [
  {
    key: '1',
    label: `Kết nối`,
    children: (
      <div>
        {/* <FuncConnect /> */}
        hello
      </div>
    ),
  },
  {
    key: '2',
    label: `Thiết lập`,
    children: (
      <div>
        {/* <FuncConfig /> */}
      </div>
    ),
  },
];

const SuricatalFreeRules = () => {
  return (
    <div>
      <div className={styled.container}>
        <div className={styled.container_title}>
          <h1>Emerging Threats Open Ruleset - SuricataFreeRules</h1>
          <div className={styled.container_title_switch}>
            <Space direction="vertical">
              <Switch checkedChildren="Bật" unCheckedChildren="Tắt" defaultChecked />
              {/* style={{ background: '#2EB100' }} */}
            </Space>
          </div>
        </div>
        <div className={styled.container_content}>
          <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
        </div>

        <div className={styled.container_btnSave}>
          <BtnSave />
        </div>
      </div>

      <div className={styled.container}>
        <div className={styled.container_title}>
          <h1>Abuse.ch SSL Blacklist Suricata RulesetFreeRules</h1>
          <div className={styled.container_title_switch}>
            <Space direction="vertical">
              <Switch style={{ background: '#2EB100' }} checkedChildren="Bật" unCheckedChildren="Tắt" defaultChecked />
            </Space>
          </div>
        </div>
        <div className={styled.container_content}>
          <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
        </div>

        <div className={styled.container_btnSave}>
          <BtnSave />
        </div>
      </div>
    </div>
  );
};

export default SuricatalFreeRules;
