import React from 'react';
import BtnSwicth from '../Button/BtnSwitch';
import styled from '../sass/OrtherConfig.module.scss';

const OrtherConfig = () => {
  return (
    <div className={styled.container}>
      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Transform Rule Action</span>
        <BtnSwicth />
      </div>

      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Transform Rule Priority keyword</span>
        <BtnSwicth />
      </div>

      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Transform Rule Threshold keyword</span>
        <BtnSwicth />
      </div>

      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Transform Rule Target keyword</span>
        <BtnSwicth />
      </div>

      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Use heuristics to set the Target Keyword value</span>
        <BtnSwicth />
      </div>

      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Insert Tags Added by User to Rule Metadata Keyword</span>
        <BtnSwicth />
      </div>

      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Insert IDSTower Rule Id to Rule Metadata Keyword</span>
        <BtnSwicth />
      </div>

      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Insert IDSTower Rule URL to Rule Metadata Keyword</span>
        <BtnSwicth />
      </div>

      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Insert IDSTower Rule Category to Rule Metadata Keyword</span>
        <BtnSwicth />
      </div>

      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Insert Extracted References to Rule Metadata Keyword</span>
        <BtnSwicth />
      </div>

      <div className={styled.container_title}>
        <span className={styled.container_title_text}>Change $EXTERNAL_NET to Any to detect lateral movement</span>
        <BtnSwicth />
      </div>
    </div>
  );
};

export default OrtherConfig;
