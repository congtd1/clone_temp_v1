import styled from '../sass/FuncConnect.module.scss';

import React from 'react';
import { Select, Space } from 'antd';
const handleChange = value => {
  console.log(`selected ${value}`);
};

const FuncConnect = () => {
  return (
    <div className={styled.container}>
      <div className={styled.container_title}>
        <span className={styled.container_title_content}>URL</span>
        <span className={styled.container_title_link}>
          <p>https://raw.githubusercontent.com/ptresearch/AttackDetection/master/pt.rules.tar.gz</p>
        </span>
      </div>
      <div className={styled.container_title}>
        <span className={styled.container_title_content}>Authentication Type</span>
        <div>
          <Space wrap>
            <Select
              defaultValue=""
              style={{
                width: 800,
              }}
              onChange={handleChange}
              options={[
                {
                  value: 'none',
                  label: 'Node',
                },
                {
                  value: 'code',
                  label: 'Secret Code',
                },
                {
                  value: 'Bacsic',
                  label: 'Bacsic Http Authentication',
                },
                {
                  value: 'disabled',
                  label: 'Disabled',
                  disabled: true,
                },
              ]}
            />
          </Space>
        </div>
      </div>
    </div>
  );
};

export default FuncConnect;
