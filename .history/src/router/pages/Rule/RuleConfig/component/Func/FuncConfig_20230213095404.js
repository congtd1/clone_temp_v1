import React from 'react';
import { CheckOutlined } from '@ant-design/icons';
import { Space, Switch } from 'antd';

const BtnSwicth = () => {
  return (
    <div>
      <Space direction="vertical">
        <Switch checkedChildren="Bật" unCheckedChildren="Tắt" defaultChecked />
      </Space>
    </div>
  );
};

export default BtnSwicth;
