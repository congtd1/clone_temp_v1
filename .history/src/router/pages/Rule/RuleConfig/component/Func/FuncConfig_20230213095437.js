import React from 'react';
import styled from '../sass/FuncConfig.module.scss';

import { Select, Space } from 'antd';
const handleChange = value => {
  console.log(`selected ${value}`);
};

const FuncConfig = () => {
  return (
    <div className={styled.container}>
      <div className={styled.container_content}>
        <span className={styled.container_content_text}>Tần suất cập nhật</span>
        <div className={styled.container_content_select}>
          <Space wrap>
            <Select
              defaultValue=""
              style={{
                width: 800,
              }}
              onChange={handleChange}
              options={[
                {
                  value: 'none',
                  label: 'Node',
                },
                {
                  value: 'code',
                  label: 'Secret Code',
                },
                {
                  value: 'Bacsic',
                  label: 'Bacsic Http Authentication',
                },
                {
                  value: 'disabled',
                  label: 'Disabled',
                  disabled: true,
                },
              ]}
            />
          </Space>
        </div>
      </div>

      <div className={styled.container_content}>
        <span className={styled.container_content_text}>Trạng thái của Rules được tạo mới</span>
        <div className={styled.container_content_select}>
          <Space wrap>
            <Select
              defaultValue=""
              style={{
                width: 800,
              }}
              onChange={handleChange}
              options={[
                {
                  value: 'none',
                  label: 'Node',
                },
                {
                  value: 'code',
                  label: 'Secret Code',
                },
                {
                  value: 'Bacsic',
                  label: 'Bacsic Http Authentication',
                },
                {
                  value: 'disabled',
                  label: 'Disabled',
                  disabled: true,
                },
              ]}
            />
          </Space>
        </div>
      </div>

      <div className={styled.container_content}>
        <span className={styled.container_content_text}>Trạng thái của Rules cập nhật mới</span>
        <div className={styled.container_content_select}>
          <Space wrap>
            <Select
              defaultValue=""
              style={{
                width: 800,
              }}
              onChange={handleChange}
              options={[
                {
                  value: 'none',
                  label: 'Node',
                },
                {
                  value: 'code',
                  label: 'Secret Code',
                },
                {
                  value: 'Bacsic',
                  label: 'Bacsic Http Authentication',
                },
                {
                  value: 'disabled',
                  label: 'Disabled',
                  disabled: true,
                },
              ]}
            />
          </Space>
        </div>
      </div>

      <div className={styled.container_content}>
        <span className={styled.container_content_text}>Trạng thái bản sửa đổi hiện thời</span>
        <div className={styled.container_content_select}>
          <Space wrap>
            <Select
              defaultValue=""
              style={{
                width: 800,
              }}
              onChange={handleChange}
              options={[
                {
                  value: 'none',
                  label: 'Node',
                },
                {
                  value: 'code',
                  label: 'Secret Code',
                },
                {
                  value: 'Bacsic',
                  label: 'Bacsic Http Authentication',
                },
                {
                  value: 'disabled',
                  label: 'Disabled',
                  disabled: true,
                },
              ]}
            />
          </Space>
        </div>
      </div>

      <div className={styled.container_content}>
        <span className={styled.container_content_text}>Import bản thay đổi của Rules cũ</span>
        <div className={styled.container_content_select}>
          <Space wrap>
            <Select
              defaultValue=""
              style={{
                width: 800,
              }}
              onChange={handleChange}
              options={[
                {
                  value: 'none',
                  label: 'Node',
                },
                {
                  value: 'code',
                  label: 'Secret Code',
                },
                {
                  value: 'Bacsic',
                  label: 'Bacsic Http Authentication',
                },
                {
                  value: 'disabled',
                  label: 'Disabled',
                  disabled: true,
                },
              ]}
            />
          </Space>
        </div>
      </div>
    </div>
  );
};

export default FuncConfig;
