import React from 'react';
import { Tabs } from 'antd';
import BtnAdddataSource from '../RuleConfig/component/Button/BtnddDataSource';
import SuricatalFreeRules from '../RuleConfig/component/Orther/SuricatalFreeRules';
import BtnSave from '../RuleConfig/component/Button/BtnSave';
import OrtherConfig from './component/OrtherConfig';

const onChange = key => {
  console.log(key);
};
const items = [
  {
    key: '1',
    label: `Quản lý Kết nối cập nhật Rules`,
    children: (
      <div>
        <SuricatalFreeRules />
      </div>
    ),
  },
  {
    key: '2',
    label: <div style={{ marginLeft: '95px' }}>Cấu hình khác</div>,
    children: (
      <div>
        <OrtherConfig />
      </div>
    ),
  },
];

const SetupRulesPJ = () => {
  return (
    <div style={{ display: 'flex', justifyContent: 'space-between', marginLeft: '31px' }}>
      <Tabs defaultActiveKey="1" items={items} onChange={onChange} />
      <BtnAdddataSource />
    </div>
  );
};

export default SetupRulesPJ;