import React from 'react';
import { Input } from 'antd';
const { TextArea } = Input;

const InputCategoryDescription = () => {
  return (
    <div>
      <div style={{ display: 'flex' }}>
        <span>Mô tả: </span>
        <TextArea style={{ width: '477px', marginLeft: '86px' }} rows={4} />
      </div>
    </div>
  );
};

export default InputCategoryDescription;
