import React from "react";
import { Input } from "antd";

const InputCategoryName = () => {
  return (
    <div style={{ display: "flex", marginBottom: "18px" }}>
      <span>
        {" "}
        Tên Category <span style={{ color: "red" }}>*</span>:
      </span>
      <Input
        style={{ width: "477px", marginLeft: "30px" }}
        placeholder="Input name category: "
      />
    </div>
  );
};

export default InputCategoryName;
