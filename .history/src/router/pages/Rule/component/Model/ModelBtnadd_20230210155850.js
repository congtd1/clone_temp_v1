import React, { useState } from 'react';
import { Button, Modal } from 'antd';
import InputCategoryName from './InputCategoryName';
import InputCategoryDescription from './InputCategoryDescription';

const ModelBtnadd = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <>
      <Button
        type="primary"
        onClick={showModal}
        style={{ marginRight: '30px', backgroundColor: '#D9D9D9', border: '1px solid #ccc', color: '#000' }}>
        Tạo mới Rules
      </Button>
      <Modal
        title="Tạo mới Category"
        style={{ textAlign: 'center' }}
        width="754px"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}>
        <div>
          <InputCategoryName />
          <InputCategoryDescription />
        </div>
      </Modal>
    </>
  );
};

export default ModelBtnadd;
