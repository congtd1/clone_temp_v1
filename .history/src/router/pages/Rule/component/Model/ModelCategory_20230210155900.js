import React, { useState } from 'react';
import { Button, Modal } from 'antd';
import InputCategoryName from './InputCategoryName';
import InputCategoryDescription from './InputCategoryDescription';

const ModelCategory = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <Button style={{ border: 'none', boxShadow: 'none' }} onClick={showModal}>
        Tạo thêm Category
      </Button>
      <Modal
        title="Tạo mới Category"
        style={{ textAlign: 'center' }}
        width="754px"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}>
        <div>
          <InputCategoryName />
          <InputCategoryDescription />
        </div>
      </Modal>
    </>
  );
};

export default ModelCategory;
