import React from 'react';
import { DownOutlined } from '@ant-design/icons';
import { Dropdown, Space } from 'antd';
import svg1 from '../../../../../assets/images/svg1.svg';

import svg2 from '../../../../../assets/images/svg2.svg';
import svg3 from '../../../../../assets/images/svg3.svg';
import svg4 from '../../../../../assets/images/svg4.svg';
import svg5 from '../../../../../assets/images/svg5.svg';
import svg8 from '../../../../../assets/images/svg8.svg';

const items = [
  {
    label: (
      <a href="">
        <img style={{ marginLeft: '6px', marginRight: '8px' }} src={svg3}></img>
        Duyệt các Rule đã chọn
      </a>
    ),
    key: '0',
  },
  {
    label: (
      <a href="">
        <img style={{ marginLeft: '6px', marginRight: '8px' }} src={svg2}></img>
        Hủy duyệt các Rule đã chọn
      </a>
    ),
    key: '1',
  },
  {
    label: (
      <a>
        <img style={{ marginLeft: '6px', marginRight: '8px' }} src={svg5}></img>
        Đặt Quá hạn các Rule đã chọn
      </a>
    ),
    key: '2',
  },
  {
    label: (
      <a>
        <img style={{ marginLeft: '6px', marginRight: '8px' }} src={svg4}></img>
        Chuyển trạng thái Xem lại với các Rule đã chọn
      </a>
    ),
    key: '3',
  },
  {
    label: (
      <a>
        <img style={{ marginLeft: '6px', marginRight: '8px' }} src={svg1}></img>
        Xóa các Rule đã chọn
      </a>
    ),
    key: '4',
  },
  {
    label: (
      <a>
        <img style={{ marginLeft: '6px', marginRight: '8px' }} src={svg8}></img>
        Thay đổi Phân loại với các Rule đã chọn
      </a>
    ),
    key: '5',
  },
  // {
  //   type: 'divider',
  // },
];

const Dropdowns = () => {
  return (
    <Dropdown
      menu={{
        items,
      }}
      trigger={['click']}>
      <a onClick={e => e.preventDefault()}>
        <Space
          type="default"
          style={{
            marginLeft: '10px',
            backgroundColor: '#D9D9D9',
            border: '1px solid #ccc',
            height: '32px',
            width: '120px',
          }}>
          <span style={{ marginLeft: '25px', textAlign: 'center', color: '#000', fontSize: '14px' }}>Tác vụ</span>
          <DownOutlined style={{ marginLeft: '8px' }} />
        </Space>
      </a>
    </Dropdown>
  );
};

export default Dropdowns;

{
  /* <Button
            type="default"
            style={{ marginLeft: '10px', backgroundColor: '#D9D9D9', border: '1px solid #ccc' }}>
            Tác vụ
            
            <DownOutlined />
            
  </Button> */
}
