import React from 'react';
import { Button, Modal } from 'antd';
import { DeleteOutlined } from '@ant-design/icons';

const ModelCategoryDelete = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <Button type="primary" onClick={showModal}>
        <DeleteOutlined />
      </Button>
      <Modal title="Xóa Category" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        <p>Bạn chắc chắn muốn xóa Category {Category_name} chứ ?</p>
        <p>Lưu ý: rules được gắn trong category sẽ không bị xóa</p>
      </Modal>
    </>
  );
};

export default ModelCategoryDelete;
