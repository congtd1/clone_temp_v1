import React, { useState } from 'react';
import { Button, Modal } from 'antd';
const ModelBtnimport = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <>
      <Button
        type="primary"
        onClick={showModal}
        style={{ marginRight: '30px', backgroundColor: '#D9D9D9', border: '1px solid #ccc', color: '#000' }}>
        Import Rules
      </Button>
      <Modal title="Basic Modal" open={isModalOpen} onOk={handleOk} onCancel={handleCancel}>
        Import Rules
      </Modal>
    </>
  );
};

export default ModelBtnimport;
