import React from 'react';
import { FilterOutlined, PlusSquareOutlined } from '@ant-design/icons';
import { Dropdown, Space } from 'antd';
import svg6 from '../../../../../assets/images/svg6.svg';
import BtnCheckbox from './BtnCheckbox';
import ModelCategory from './ModelCategory';

const items = [
  {
    label: <a style={{ textAlign: 'center', display: 'block' }}>Bộ lọc phân loại</a>,
    key: '0',
  },
  {
    type: 'divider',
  },
  {
    label: (
      <a style={{ display: 'flex' }}>
        <BtnCheckbox />
        Category 1 (1 rules)
      </a>
    ),
    key: '1',
  },
  {
    label: (
      <a style={{ display: 'flex' }}>
        <BtnCheckbox />
        Category 2 (2 rules)
      </a>
    ),
    key: '2',
  },
  {
    label: (
      <a style={{ display: 'flex' }}>
        <BtnCheckbox />
        SSLBL (143 rules)
      </a>
    ),
    key: '3',
  },
  {
    label: (
      <a style={{ display: 'flex' }}>
        <BtnCheckbox />
        test (143 rules)
      </a>
    ),
    key: '4',
  },
  {
    type: 'divider',
  },
  {
    label: (
      <div style={{ display: 'flex' }}>
        <PlusSquareOutlined style={{ marginRight: '6px', marginTop: '8px' }} />
        <ModelCategory />
      </div>
    ),
    key: '5',
  },
];

const BtnCategory = () => {
  return (
    <div>
      <Dropdown
        menu={{
          items,
        }}
        trigger={['click']}>
        <a onClick={e => e.preventDefault()}>
          <Space>
            Category
            <FilterOutlined style={{ fontSize: '22px', marginTop: '4px' }} />
          </Space>
        </a>
      </Dropdown>
    </div>
  );
};

export default BtnCategory;
