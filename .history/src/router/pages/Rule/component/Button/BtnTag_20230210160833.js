import React from 'react';
import { Button, Dropdown, Space } from 'antd';
import svg6 from '../../../../../assets/images/svg6.svg';

import { FilterOutlined } from '@ant-design/icons';

const items = [
  {
    label: <a style={{ textAlign: 'center', display: 'block' }}>Bộ lọc Tag</a>,
    key: '0',
  },
  {
    type: 'divider',
  },
  {
    label: (
      <a>
        Action:
        <Button
          type="default"
          style={{
            borderRadius: '6px',
            backgroundColor: '#3FADDC',
            color: '#000',
            fontWeight: 400,
            marginRight: '10px',
            border: 'none',
            marginLeft: '6px',
          }}>
          aleft
        </Button>
      </a>
    ),
    key: '1',
  },
  {
    label: <a>Protocol: dns, ftp, httpt, icmp, ip, mms, smb, smtp, ssh, tcp, tls, udp</a>,
    key: '2',
  },
  {
    label: (
      <a>
        Class Type: attempted-admin, attempted-dos, attempted-recon, attempted-user, bad-unknown, coin-mining,
        command-and-control, credential-theft, default-login-attempt, denial-of-service, domain-c2, exploit-kit,
        external-ip-check, misc-activity, misc-attack, network-scan, non-standard-protocol, not-suspicious, policy-violation,
        protocol-command-decode, pup-activity, rpc-portmap-decode, shellcode-detect, social-engineering, string-detect,
        successful-admin, successful-recon-largescale, successful-recon-limited, successful-user, suspicious-filename-detect,
        suspicious-login, system-call-detect, targeted-activity, trojan-activity, unknown, unsuccessful-user,
        web-application-activity, web-application-attack
      </a>
    ),
    key: '3',
  },
];

const BtnTag = () => {
  return (
    <div>
      <Dropdown
        overlayStyle={{ width: '754px' }}
        menu={{
          items,
        }}
        trigger={['click']}>
        <a onClick={e => e.preventDefault()}>
          <Space>
            Tag
            <FilterOutlined style={{ fontSize: '22px', marginTop: '4px' }} />
          </Space>
        </a>
      </Dropdown>
    </div>
  );
};

export default BtnTag;
