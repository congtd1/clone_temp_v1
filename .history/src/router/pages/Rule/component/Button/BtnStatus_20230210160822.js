import React from 'react';
import { DownOutlined, FilterOutlined } from '@ant-design/icons';
import { Button, Dropdown, Space } from 'antd';
import svg6 from '../../../../../assets/images/svg6.svg';
import BtnCheckbox from './BtnCheckbox';

const items = [
  {
    label: (
      <div style={{ display: 'flex' }}>
        <BtnCheckbox />
        <Button
          type="default"
          style={{ borderRadius: '6px', backgroundColor: '#2EB100', color: '#fff', fontWeight: 700, border: 'none' }}>
          Duyệt
        </Button>
      </div>
    ),
    key: '0',
  },
  {
    label: (
      <div style={{ display: 'flex' }}>
        <BtnCheckbox />
        <Button
          type="default"
          style={{ borderRadius: '6px', backgroundColor: '#DE2020', color: '#fff', fontWeight: 700, border: 'none' }}>
          Hủy
        </Button>
      </div>
    ),
    key: '1',
  },
  {
    label: (
      <div style={{ display: 'flex' }}>
        <BtnCheckbox />
        <Button
          type="default"
          style={{ borderRadius: '6px', backgroundColor: '#B4B3B3', color: '#fff', fontWeight: 700, border: 'none' }}>
          Quá hạn
        </Button>
      </div>
    ),
    key: '2',
  },
  {
    label: (
      <div style={{ display: 'flex' }}>
        <BtnCheckbox />
        <Button
          type="default"
          style={{ borderRadius: '6px', backgroundColor: '#C9CC1F', color: '#fff', fontWeight: 700, border: 'none' }}>
          Xem lại
        </Button>
      </div>
    ),
    key: '3',
  },
];

const BtnStatus = () => {
  return (
    <div>
      <Dropdown
        menu={{
          items,
        }}
        trigger={['click']}>
        <a onClick={e => e.preventDefault()}>
          <Space>
            Trạng thái
            <FilterOutlined style={{ fontSize: '22px', marginTop: '4px' }} />
          </Space>
        </a>
      </Dropdown>
    </div>
  );
};

export default BtnStatus;
