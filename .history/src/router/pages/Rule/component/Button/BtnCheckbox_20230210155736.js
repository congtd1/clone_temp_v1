import React from 'react';
import { Checkbox } from 'antd';

const onChange = e => {
  console.log(`checked = ${e.target.checked}`);
};

const BtnCheckbox = () => {
  return (
    <div>
      <Checkbox style={{marginRight: '6px'}} onChange={onChange}></Checkbox>
    </div>
  );
};

export default BtnCheckbox;
