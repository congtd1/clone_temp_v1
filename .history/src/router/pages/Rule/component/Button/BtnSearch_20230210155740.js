import React from 'react';
import { AudioOutlined } from '@ant-design/icons';
import { Input, Space } from 'antd';
const { Search } = Input;
const suffix = (
  <AudioOutlined
    style={{
      fontSize: 16,
      color: '#1890ff',
    }}
  />
);
const onSearch = value => console.log(value);

const BtnSearch = () => {
  return (
    <Space direction="vertical">
      <Search
        placeholder="Nhập từ khóa để tìm kiếm"
        onSearch={onSearch}
        style={{
          width: 632,
          marginLeft: '50px',
        }}
      />
    </Space>
  );
};

export default BtnSearch;
