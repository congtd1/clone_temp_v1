import Home from "../icons/icons8-home-98 1.svg";
import Statistics from "../icons/icons8-statistics-100 1.svg";
import Circled from "../icons/icons8-circled-menu-100 1.svg";
import Rule from "../icons/icons8-rules-100 1.svg";
import Progress from "../icons/icons8-progress-indicator-100 1.svg";
import User from "../icons/icons8-user-100 1.svg";
import Preview from "../icons/icons8-preview-pane-100 1.svg";
import Pen from "../icons/icons8-pen-100 1.svg";
import SERVERS from "../icons/icons8-servers-group-100 1.svg";
import MODULES from "../icons/icons8-module-100 1.svg";
import SETTINGS from "../icons/icons8-setting-100 1.svg";
import PEOPLES from "../icons/icons8-people-100 1.svg";
import FOLDER from "../icons/icons8-user-folder-100 1.svg";

import {
  DASHBOARD,
  STATISTICS,
  SERVICES,
  RULES,
  WARNINGS,
  ADMINSTRATIONS,
  CENSOR,
  CGATE,
  DEEPANALYST,
  RULEMANAGE,
  RULECONFIG,
  ACCOUNTMANAGE,
  RULESGROUPMANAGE,
  DATAMANAGE,
  CONFIGCHART,
  RULESSETTING,
} from "./itemsContants";
import styles from "../layouts/Layout.module.scss";

const menuItems = [
  {
    key: DASHBOARD,
    label: <div className={styles.nav}>Bảng điều khiển</div>,
    icon: <img alt="img" className={styles.img} src={Home} />,
  },
  {
    key: STATISTICS,
    label: <div className={styles.nav}>Báo cáo & Phân tích</div>,
    icon: <img alt="img" className={styles.img} src={Statistics} />,
    children: [
      {
        key: DATAMANAGE,
        label: <div className={styles.nav}>Theo dõi số liệu</div>,
        icon: <img alt="img" className={styles.img} src={Preview} />,
      },
      {
        key: CONFIGCHART,
        label: <div className={styles.nav}>Cấu hình biểu đồ</div>,
        icon: <img alt="img" className={styles.img} src={Pen} />,
      },
    ],
  },
  {
    key: SERVICES,
    label: <div className={styles.nav}>Dịch vụ</div>,
    icon: <img alt="img" className={styles.img} src={Circled} />,
    children: [
      {
        key: CENSOR,
        label: <div className={styles.nav}>Censor</div>,
        icon: <img alt="img" className={styles.img} src={SERVERS} />,
      },
      {
        key: CGATE,
        label: <div className={styles.nav}>Cgate</div>,
        icon: <img alt="img" className={styles.img} src={SERVERS} />,
      },
      {
        key: DEEPANALYST,
        label: <div className={styles.nav}>Deep Analyst</div>,
        icon: <img alt="img" className={styles.img} src={SERVERS} />,
      },
    ],
  },
  {
    key: RULES,
    label: <div className={styles.nav}>Rules</div>,
    icon: <img alt="img" className={styles.img} src={Rule} />,
    children: [
      {
        key: RULEMANAGE,
        label: <div className={styles.nav}>Quản lý Rules</div>,
        icon: <img alt="img" className={styles.img} src={MODULES} />,
      },
      {
        key: RULESSETTING,
        label: <div className={styles.nav}>Thiết lập Rules</div>,
        icon: <img alt="img" className={styles.img} src={SETTINGS} />,
      },
    ],
  },
  {
    key: WARNINGS,
    label: <div className={styles.nav}>Cảnh báo</div>,
    icon: <img alt="img" className={styles.img} src={Progress} />,
  },
  {
    key: ADMINSTRATIONS,
    label: <div className={styles.nav}>Quản trị</div>,
    icon: <img alt="img" className={styles.img} src={User} />,
    children: [
      {
        key: ACCOUNTMANAGE,
        label: <div className={styles.nav}>Quản lý tài khoản</div>,
        icon: <img alt="img" className={styles.img} src={PEOPLES} />,
      },
      {
        key: RULESGROUPMANAGE,
        label: <div className={styles.nav}>Quản lý nhóm quyền </div>,
        icon: <img alt="img" src={FOLDER} />,
      },
    ],
  },
];

export default menuItems;
