import { SERVICE_PATHS } from "../../constants/paths";
import { get, update, remove, post } from "../../api/ApiBase";

const EventService = {
  getEvent: (body) => {
    var filterString = "?";
    Object.keys(body).map((index) => {
      if (filterString != "?" && body[index] != null) {
        filterString += `&${index}=${body[index]}`;
      } else if (body[index] != null) {
        filterString += `${index}=${body[index]}`;
      }
    });
    if (filterString == "?") {
      get(SERVICE_PATHS.EVENT.GET_EVENT);
    } else {
      get(`${SERVICE_PATHS.EVENT.GET_EVENT}${filterString}`);
    }
  },
  getDetail: (id) => get(`${SERVICE_PATHS.EVENT.GET_DETAIL}${id}`),
};

export default EventService;
