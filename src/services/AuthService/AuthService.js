import { SERVICE_PATHS } from "../../constants/paths";

import { post , get } from "../../api/ApiBase";

const AuthService = {

    signin:body => post(SERVICE_PATHS.AUTH.SIGN_IN,body),
    getInfo:body => get(SERVICE_PATHS.AUTH.GET_INFO)

}

export default AuthService;
