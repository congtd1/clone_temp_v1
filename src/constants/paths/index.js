

export const SERVICE_PATHS = {
    AUTH: {
        SIGN_IN: '/AIViewService/api/auth/signin',
        GET_INFO: '/AIViewService/api/aiview/getUserInfo'
    },
    EVENT: {
        GET_EVENT: '/api/cctv/get',
        GET_DETAIL: '/AIViewService/api/aiview/getReportByEventId/',
    },
    APPLICATION: {
        GET_APPLICATION: '/AIViewService/api/application/getAllApplication'
    },
    CAMERA: {
        GET_CAMERA: '/api/vms/camera/get'
    },
    REGION: {
        GET_REGION: '/AIViewService/api/aiview/getAllRegion'
    },
    IDCARD: {
        GET_ID_CARD: '/api/ocr/get/'
    }
};
