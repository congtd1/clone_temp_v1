

const TYPE_ACTION = {
    EVENT: {
        GET: 'GET_EVENT',
        DETAIL: 'GET_DETAIL'
    },
    APPLICATION: {
        GET: 'GET_APPLICATION'
    },
    CAMERA: {
        GET: 'GET_CAMERA'
    },
    REGION: {
        GET: 'GET_REGION'
    },
    AUTH: {
        GET_INFO: 'GET_INFO'
    },
    ID_CARD: {
        GET_ID_CARD: 'GET_ID_CARD'
    }
}
export default TYPE_ACTION;
