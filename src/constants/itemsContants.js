const PATH = {
    EVENT: "event",
    DASHBOARD: "dashboard",
    SEEK:"seek",
    FACE:"face",
    LICENSE_PLATE:"license plate",
    SIGNIN:"signin",
    PROFILE:"profile"
}
export default PATH;
