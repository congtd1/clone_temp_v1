import { useNavigate, Route, Routes, useLocation } from "react-router-dom";
import * as React from "react";

//component
import PageLoader from "../common/component/PageLoader";

// Util
import PATH from "../constants/itemsContants";
import SignIn from "../screen/pages/Signin/SignIn";

// Routes
const Timeout = 300;

const Dashboard = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(
      () => resolve(import("../screen/pages/Dashboard/Dashboard.js")),
      Timeout
    );
  });
});

const Event = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(
      () => resolve(import("../screen/pages/Event/Event.js")),
      Timeout
    );
  });
});

const FaceSearch = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(
      () => resolve(import("../screen/pages/FaceSearch/FaceSearch.js")),
      Timeout
    );
  });
});

const LicensePlateSearch = React.lazy(() => {
  return new Promise((resolve) => {
    setTimeout(
      () =>
        resolve(
          import("../screen/pages/LicensePlateSearch/LicensePlateSearch.js")
        ),
      Timeout
    );
  });
});

const Signin = React.lazy(() => {
    return new Promise((resolve) => {
        setTimeout(() => resolve(import("../screen/pages/Signin/SignIn")), Timeout);
    });
});

const Profile = React.lazy(() => {
  return new Promise((resolve) => {
      setTimeout(() => resolve(import("../screen/pages/Profile/Profile.js")), Timeout);
  });
});


const Router = () => {
  const location = useLocation();
  const navigate = useNavigate();
  const pathSignIn = `/${PATH.SIGNIN}`;
  const token = localStorage.getItem("token");

  const Redirect = () => {
    if ((pathSignIn.includes(location.pathname) || location.pathname == "/" || location.pathname == " ") && !token) {
      navigate(PATH.SIGNIN);
    } else if (token) {
      navigate(PATH.EVENT);
    }
  };

  React.useEffect(() => {
    Redirect();
  }, []);

  return (
    <Routes>
      <Route
        path={PATH.DASHBOARD}
        element={
          <React.Suspense fallback={<PageLoader />}>
            <Dashboard />
          </React.Suspense>
        }
      />
      <Route
        path={PATH.EVENT}
        element={
          <React.Suspense fallback={<PageLoader />}>
            <Event />
          </React.Suspense>
        }
      />
      <Route
        path={PATH.FACE}
        element={
          <React.Suspense fallback={<PageLoader />}>
            <FaceSearch />
          </React.Suspense>
        }
      />
      <Route
        path={PATH.LICENSE_PLATE}
        element={
          <React.Suspense fallback={<PageLoader />}>
            <LicensePlateSearch />
          </React.Suspense>
        }
      />
      <Route
        path={PATH.SIGNIN}
        element={
          <React.Suspense fallback={<PageLoader />}>
            <SignIn />
          </React.Suspense>
        }
      />
      <Route
        path={PATH.PROFILE}
        element={
          <React.Suspense fallback={<PageLoader />}>
            <Profile />
          </React.Suspense>
        }
      />
    </Routes>
  );
};
export default Router;
