import ApiService from "../services/ConfigService/ApiService";

export const get = (url) => ApiService().get(url);

export const update = (url, body) => ApiService().put(url, body);

export const remove = (url) => ApiService().delete(url);

export const post = (url,body) => ApiService().post(url,body);

