import {
    call,
    put,
    takeEvery,
} from "redux-saga/effects";

//Api 
import EventService from "../services/EventService/EventService.js";

import TYPE_ACTION from "../constants/TypeAction.js";

//Slice 
import { getEvent, getDetailEvent } from "../redux/slice/EventSlice.js";

//----Worker
function* doGetEventSaga (action) {
    const body = action.payload;
    try {
        const response = yield call(EventService.getEvent,body);
        console.log("response",response);
        yield put(getEvent(data));
    } catch (error) {
        console.log("Error fetching: ", error);
    }
};

function* doGetEventDetailSaga(action) {
    const id = action.payload;
    try {
        const response = yield call(EventService.getDetail,id);
        const data = response;
        yield put(getDetailEvent(data));
    } catch (error){
        console.log("Error fetching detail ",error);
    }
};


//----Watcher
function* watchGetEventSaga() {
    yield takeEvery(TYPE_ACTION.EVENT.GET, doGetEventSaga);
};

function* watchGetEventDetailSaga() {
    yield takeEvery(TYPE_ACTION.EVENT.DETAIL,doGetEventDetailSaga);
};

export {
    watchGetEventSaga,
    watchGetEventDetailSaga
};