import {
    call,
    put,
    takeEvery,
} from "redux-saga/effects";

//Api 
import IdCardService from "../services/IDCardService/IdCardService.js";

import TYPE_ACTION from "../constants/TypeAction.js";

//Slice 
import { getIdCard } from "../redux/slice/IDCardSlice";

//----Worker
function* doGetIdCardSaga (action) {
    const body = action.payload;
    try {
        const response = yield call(IdCardService.getIdCard,body);
        console.log("response id card",response);
        yield put(getIdCard(data));
    } catch (error) {
        console.log("Error fetching: ", error);
    } 
};

//----Watcher 
function* watchGetIdCardSaga() {
    yield takeEvery(TYPE_ACTION.ID_CARD.GET_ID_CARD, doGetIdCardSaga);
};

export {
    watchGetIdCardSaga
}