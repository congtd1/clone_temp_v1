// saga effects
import { all } from "redux-saga/effects";

//Event 
import {
    watchGetEventSaga,
    watchGetEventDetailSaga
} from "../saga/EventSaga";

//Application
import {
    watchGetApplicationSaga
} from "../saga/ApplicationSaga";

//Camera
import {
    watchGetCameraSaga
} from "../saga/CameraSaga";

//Region
import {
    watchGetRegionSaga
} from "../saga/RegionSaga";

//Auth
import {
    watchGetAuthInfoSaga
} from "../saga/AuthSaga";

//ID Card
import {
    watchGetIdCardSaga
} from "../saga/IDCardSaga";

export default function* rootSaga() {
    yield all([
        //----Event
        watchGetEventSaga(),
        watchGetEventDetailSaga(),
        //----Application
        watchGetApplicationSaga(),
        //-----Camera
        watchGetCameraSaga(),
        //-----Region
        watchGetRegionSaga(),
        //-----Auth
        watchGetAuthInfoSaga(),
        //-----ID Card
        watchGetIdCardSaga()
    ]);
};
