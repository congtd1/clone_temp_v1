import {createSlice} from '@reduxjs/toolkit';

export const eventSlice = createSlice({
    name: 'events',
    initialState: {
        event: [],
        totalRecord:0,
        detail:{}
    },
    reducers: {
        getEvent: (state,action) => {
            const { data } = action.payload;
            return {
                event: data.dataList,
                totalRecord:data.totalRecord,
            }
        },
        getDetailEvent:(state,action) => {
            const { data } = action.payload;
            return {
                detail:data.data,
                event: state.event,
                totalRecord:state.totalRecord
            }
        }
    }
});

export const { getEvent, getDetailEvent } = eventSlice.actions;

export const selectEventData = state => state.events.event;
export const selectEventTotalRecord = state => state.events.totalRecord;
export const selectEventDetail = state => state.events.detail;

export default eventSlice.reducer;