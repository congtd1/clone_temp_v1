import { Button, Checkbox, Form, Input } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router";

// Utils
import AuthService from "../../../../services/AuthService/AuthService";

// Style
import styles from "./FormLogin.module.scss";

import TYPE_ACTION from "../../../../constants/TypeAction";

const FormLogin = () => {
    const navigate = useNavigate();

    //-----
    const dispatch = useDispatch();

    const loadUserInfo = () => {
        dispatch({
            type:TYPE_ACTION.AUTH.GET_INFO
        });
    }

    const onFinish = async (values) => {
        try {
            const response = await AuthService.signin({
                username: values.username,
                password: values.password,
            });
            const { data } = response;
            localStorage.setItem("token",data.data.token);
            localStorage.setItem("refreshToken",data.data.refreshToken);
            loadUserInfo();
            navigate("/event");
        } catch (error) {
            console.log(error.response);
        }
    };

    const onFinishFailed = (errorInfo) => {
        console.log("Failed:", errorInfo);
    };

    return (
        <Form
            name="basic"
            labelCol={{
                span: 7,
                offset: 1,
            }}
            wrapperCol={{
                span: 14,
                offset: 1,
            }}
            style={{
                maxWidth: 600,
            }}
            initialValues={{
                remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
        >
            <Form.Item
                label={<h3 className={styles.field}>Tên đăng nhập</h3>}
                name="username"
                rules={[
                    {
                        required: true,
                        message: <span className={styles.message}>Tên đăng nhập đang trống!</span>,
                    },
                ]}
            >
                <Input placeholder="Tên đăng nhập ..." />
            </Form.Item>

            <Form.Item
                label={<h3 className={styles.field}>Mật khẩu</h3>}
                name="password"
                rules={[
                    {
                        required: true,
                        message: <span className={styles.message}>Mật khẩu đang trống!</span>,
                    },
                ]}
            >
                <Input.Password placeholder="Mật khẩu ..." />
            </Form.Item>

            <Form.Item
                name="remember"
                valuePropName="checked"
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Checkbox className={styles.remember}>Remember me</Checkbox>
            </Form.Item>

            <Form.Item
                wrapperCol={{
                    offset: 8,
                    span: 16,
                }}
            >
                <Button className={styles.button} type="primary" htmlType="submit">
                    Đăng nhập
                </Button>
            </Form.Item>
        </Form>
    );
};

export default FormLogin;
