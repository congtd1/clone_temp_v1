import { Button } from "antd";
import { useNavigate } from "react-router";
import FormLogin from "./FormLogin";
import styles from "./SignIn.module.scss";

const SignIn = () => {
  const navigate = useNavigate();
  return (
    <div className={styles.container}>
      <div className={styles.wrapper}>
        <h3 className={styles.header}>Đăng nhập</h3>
        <div className={styles.form}>
          <FormLogin></FormLogin>
        </div>
      </div>
    </div>
  );
};

export default SignIn;
