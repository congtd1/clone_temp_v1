import FormProfile from "./components/FormProfile/FormProfile";
import UploadImage from "./components/UploadImage/UploadImage";


const Profile = () => {
    return (
        <>
            <UploadImage></UploadImage>
            <FormProfile></FormProfile>
        </>
    )
};

export default Profile;