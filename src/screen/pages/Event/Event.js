import { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import TYPE_ACTION from "../../../constants/TypeAction";
import {
  selectEventData,
  selectEventTotalRecord,
} from "../../../redux/slice/EventSlice";
import { Input, Menu, Table, Tooltip } from "antd";
import {
  ExclamationCircleOutlined,
  FileImageOutlined,
  OrderedListOutlined,
  SearchOutlined,
} from "@ant-design/icons";
import styles from "./Event.module.scss";
import SearchComponent from "./components/SearchComponent";
import Images from "./components/Images/Images";
const Event = () => {
  const dispatch = useDispatch();
  const eventData = useSelector(selectEventData);
  const eventTotalRecord = useSelector(selectEventTotalRecord);
  const [selectedRowKeys, setSelectedRowKeys] = useState([]);
  const [loading, setLoading] = useState(false);
  const filterRef = useRef({
    soCmts:null,
    camera_ids:null,
    start_time:null,
    end_time:null,
    distinct:null
  });

  const data = [];
  const columns = [
    {
      title: <div style={{ textAlign: "center" }}>ID</div>,
      dataIndex: "eventId",
    },
    {
      title: <div style={{ textAlign: "center" }}>Tên sự kiện</div>,
      dataIndex: "eventName",
    },
    {
      title: (
        <div style={{ textAlign: "center" }}>Thời gian bắt đầu sự kiện</div>
      ),
      dataIndex: "startTime",
    },
    {
      title: (
        <div style={{ textAlign: "center" }}>Thời gian kết thúc sự kiện</div>
      ),
      dataIndex: "endTime",
    },
    {
      title: <div style={{ textAlign: "center" }}>Tên bài</div>,
      dataIndex: "applicationName",
    },
    {
      title: <div style={{ textAlign: "center" }}>Tên Camera</div>,
      dataIndex: "cameraName",
    },
    {
      title: <div style={{ textAlign: "center" }}>Khu vực lắp đặt</div>,
      dataIndex: "regionName",
    },
    {
      title: <div style={{ textAlign: "center" }}>Địa chỉ</div>,
      dataIndex: "address",
    },
    {
      title: <div style={{ textAlign: "center" }}>Chức năng</div>,
      dataIndex: "function",
    },
  ];
  const onSelectChange = (newSelectedRowKeys) => {
    console.log("selectedRowKeys changed: ", newSelectedRowKeys);
    setSelectedRowKeys(newSelectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const hasSelected = selectedRowKeys.length > 0;
  const loadData = () => {
    console.log(filterRef.current);
    dispatch({
      type: TYPE_ACTION.EVENT.GET,
      payload: filterRef.current,
    });
  };
  useEffect(() => {
    loadData();
  }, []);
  eventData?.map((dataIndex) => {
    data.push({
      key: dataIndex.eventId,
      eventId: <div className={styles.wrapper}>{dataIndex.eventId}</div>,
      eventName: <div className={styles.wrapper}>{dataIndex.eventName}</div>,
      startTime: <div className={styles.wrapper}>{dataIndex.startTime}</div>,
      endTime: <div className={styles.wrapper}>{dataIndex.endTime}</div>,
      applicationName: (
        <div className={styles.wrapper}>{dataIndex.applicationName}</div>
      ),
      cameraName: <div className={styles.wrapper}>{dataIndex.cameraName}</div>,
      regionName: <div className={styles.wrapper}>{dataIndex.regionName}</div>,
      address: <div className={styles.wrapper}>{dataIndex.address}</div>,
      function: (
        <div className={styles.wrapper}>
          <Tooltip placement="top" title={"Thông tin"}>
            <ExclamationCircleOutlined className={styles.icon} />
          </Tooltip>
        </div>
      ),
    });
  });
  const [current, setCurrent] = useState("Details");
  const onClick = (e) => {
    setCurrent(e.key);
  };
  const items = [
    {
      label: "Chi tiết",
      key: "Details",
      icon: <OrderedListOutlined className={styles.icons} />,
    },
    {
      label: "Hình ảnh",
      key: "Images",
      icon: <FileImageOutlined className={styles.icons} />,
    },
  ];
  return (
    <>
      <div style={{ position: "relative" }}>
        <SearchComponent
          loading={loading}
          setLoading={(values) => setLoading(values)}
          loadData={loadData}
          filterRef={filterRef}
        ></SearchComponent>
        <Menu
          onClick={onClick}
          selectedKeys={[current]}
          mode="horizontal"
          items={items}
          className={styles.menu}
        />
      </div>
      {current === "Details" ? (
        <Table
          pagination={{
            position: ["bottomCenter"],
            pageSize: filterRef.current.pageSize,
            total: eventTotalRecord,
            showQuickJumper: true,
            pageSizeOptions:["12","16","20"]
          }}
          rowSelection={rowSelection}
          columns={columns}
          dataSource={data}
          bordered
          loading={loading}
          onChange={(e) => {
            console.log(e.current);
            filterRef.current.pageIndex = e.current;
            filterRef.current.pageSize = e.pageSize;
            console.log(filterRef.current);
            loadData();
          }}
        ></Table>
      ) : (
        <Images></Images>
      )}
    </>
  );
};

export default Event;
