import { VideoCameraOutlined } from "@ant-design/icons";
import { Empty, Input, Modal } from "antd";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  selectEventData,
  selectEventDetail,
} from "../../../../../redux/slice/EventSlice";
import styles from "./Images.module.scss";
import TYPE_ACTION from "../../../../../constants/TypeAction.js";

const Images = () => {
  //-----Selectors
  const eventData = useSelector(selectEventData);
  const eventDetailData = useSelector(selectEventDetail);

  //---- Dispatch
  const dispatch = useDispatch();

  //-----State
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [modalData, setModalData] = useState({});

  //----Functions
  const showModal = (data) => {
    setIsModalOpen(true);
    setModalData(data);
    dispatch({
      type: TYPE_ACTION.EVENT.DETAIL,
      payload: data.eventId,
    });
  };

  const handleOk = () => {
    setIsModalOpen(false);
  };

  const handleCancel = () => {
    setIsModalOpen(false);
  };

  return (
    <div className={styles.container}>
      <Modal
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        width={"70%"}
      >
        <h3>{`Sự kiện chi tiết - ${modalData?.eventName}`}</h3>
        <div className={styles.modalWrapper}>
          <div className={styles.item}>
            <h4>Tên Camera</h4>
            <Input value={modalData?.cameraName}></Input>
          </div>
          <div className={styles.item}>
            <h4>Khu vực lắp đặt</h4>
            <Input value={modalData?.regionName}></Input>
          </div>
        </div>
        <div className={styles.modalWrapper}>
          <div className={styles.item}>
            <h4>Địa chỉ</h4>
            <Input value={modalData?.address}></Input>
          </div>
          <div className={styles.item}>
            <h4 className={styles.header}>Thời gian từ - đến</h4>
            <div className={styles.dateWrapper}>
              <Input
                className={styles.date}
                value={modalData?.startTime}
              ></Input>
              <b> đến </b>
              <Input className={styles.date} value={modalData?.endTime}></Input>
            </div>
          </div>
        </div>
        {modalData?.eventFileList?.length && modalData?.eventFileList?.map((eventFileListIndex) => {
          console.log(eventFileListIndex)
          return (
            <span>{eventFileListIndex.filePath}</span>
          )
        })}
        <Empty description={<h2>Không thể tải hình ảnh</h2>}></Empty>
      </Modal>
      {eventData.length ? (
        eventData?.map((index) => {
          return (
            <>
              <div
                className={styles.wrapper}
                key={index.eventId}
                onClick={() => showModal(index)}
              >
                {index.filePath ? (
                  <img src={index.filePath} className={styles.image} />
                ) : (
                  <div className={styles.image}>
                    <VideoCameraOutlined className={styles.camera} />
                  </div>
                )}
                <h3 className={styles.title}>{index.eventName}</h3>
                <h4 className={styles.title}>{index.startTime}</h4>
              </div>
            </>
          );
        })
      ) : (
        <Empty></Empty>
      )}
    </div>
  );
};

export default Images;
