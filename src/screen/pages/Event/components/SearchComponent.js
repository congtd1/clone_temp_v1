import { Button, DatePicker, Select, Tag } from "antd";
import { useDispatch, useSelector } from "react-redux";
import TYPE_ACTION from "../../../../constants/TypeAction";
import { selectCameraData } from "../../../../redux/slice/CameraSlice";
import { selectIdCardData } from "../../../../redux/slice/IDCardSlice";
import styles from "./SearchComponent.module.scss";

const SearchComponent = ({ loadData, filterRef }) => {
  const idCardOptions = [];
  const idCameraOptions = [];
  for (let i = 10; i < 36; i++) {
    idCameraOptions.push({
      value: i.toString(36) + i,
      label: i.toString(36) + i,
    });
    idCardOptions.push({
      value: i.toString(36) + i,
      label: i.toString(36) + i,
    });
  }

  const tagRender = (props) => {
    const { label, closable } = props;
    return (
      <Tag
        color={"blue"}
        closable={closable}
        style={{
          marginRight: 3,
        }}
      >
        {label}
      </Tag>
    );
  };

  const { Option } = Select;
  const { RangePicker } = DatePicker;

  //----Selectors
  const cameraData = useSelector(selectCameraData);
  const idCardData = useSelector(selectIdCardData);

  //-----Dispatchs
  const dispatch = useDispatch();

  const loadCamera = () => {
    dispatch({
      type: TYPE_ACTION.CAMERA.GET,
      payload: {},
    });
  };

  //-----Functions
  const addFilterCmts = (value, event) => {
    console.log(value);
    filterRef.current.soCmts = value.join(",");
  };

  const addFilterCameraIds = (value, event) => {
    filterRef.current.camera_ids = value.join(",");
  };

  const addFilterTimeEvent = (value, event) => {
    filterRef.current.start_time = event[0];
    filterRef.current.end_time = event[1];
  };

  const filterData = () => {
    console.log("AAAAAAAAAAAAAAAA");
    loadData();
  };

  return (
    <div className={styles.container}>
      <div className={styles.container}>
        <div className={styles.selectsWrapper}>
          <div className={styles.selects}>
            <h4 className={styles.name}>Số chứng minh thư :</h4>
            <Select
              mode="multiple"
              placeholder="Nhập số CMT ..."
              defaultValue={[]}
              onChange={addFilterCmts}
              tagRender={tagRender}
              style={{
                width: "100%",
              }}
              options={idCardOptions}
            />
          </div>
          <div className={styles.selects}>
            <h4 className={styles.name}>Camera ID :</h4>
            <Select
              onClick={() => loadCamera()}
              mode="multiple"
              placeholder="Nhập Camera ID ..."
              defaultValue={[]}
              onChange={addFilterCameraIds}
              tagRender={tagRender}
              style={{
                width: "100%",
              }}
              options={idCameraOptions}
            />
          </div>
          <div className={styles.selects}>
            <h4 className={styles.name}>Lọc các sự kiện trùng lặp :</h4>
            <Select
              className={styles.select}
              onChange={(value) => (filterRef.current.distinct = value)}
              defaultValue={null}
              placeholder={"Lọc sự kiện trùng lặp . . ."}
            >
              <Option value={true}>Có</Option>
              <Option value={false}>Không</Option>
            </Select>
          </div>
        </div>
        <div className={styles.dateWrapper}>
          <h3>Thời gian bắt đầu sự kiện - Thời gian kết thúc sự kiện :</h3>
          <RangePicker
            showTime
            className={styles.picker}
            onChange={(values, event) => addFilterTimeEvent(values, event)}
          />
        </div>
        <div className={styles.button}>
          <Button onClick={() => filterData()} type="primary">
            Tìm kiếm
          </Button>
        </div>
      </div>
    </div>
  );
};

export default SearchComponent;
