import { BellFilled, ProfileOutlined } from "@ant-design/icons";
import {
  Avatar,
  Badge,
  Breadcrumb,
  Dropdown,
  Layout,
  Menu,
  Select,
  Space,
  theme,
} from "antd";
import React, { useState } from "react";
import { Link, useLocation, useNavigate } from "react-router-dom";
import LOGO from "../../assets/images/logo.svg";
import {
  DashboardOutlined,
  EyeOutlined,
  FileSearchOutlined,
  MehOutlined,
  UserOutlined,
} from "@ant-design/icons";

//-----LOGO
import vietnam from "../../assets/images/co_viet_nam.jpg";
import english from "../../assets/images/co_nuoc_anh.jpg"

// Utils
import PATH from "../../constants/itemsContants";

// styles
import "./styles.css";
import styles from "./Layout.module.scss";
import { selectAuthInfo } from "../../redux/slice/AuthSlice";
import { useSelector } from "react-redux";
import { useTranslation } from "react-i18next";
import i18n from "../../i18n/translate.js";

const { Header, Content, Sider } = Layout;
const LayoutWrapper = ({ children }) => {
  const [collapsed, setCollapsed] = useState(false);
  const navigate = useNavigate();
  const location = useLocation();
  const { t } = useTranslation();

  const SignOut = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("refreshToken");
    navigate(`/${PATH.SIGNIN}`);
  };

  //-----Selectors
  const user = JSON.parse(localStorage.getItem("user"));

  const items = [
    {
      key: "1",
      label: "Đổi mật khẩu",
    },
    {
      key: "2",
      label: "Đổi thông tin",
    },
    {
      key: "3",
      label: <h4 onClick={SignOut}>Đăng xuất</h4>,
    },
  ];

  const {
    token: { colorBgContainer },
  } = theme.useToken();
  const onClick = (e) => {
    console.log(e.key);
    navigate(e.key);
  };
  const { Option } = Select;
  const pathSignIn = "/" + PATH.SIGNIN;
  if (pathSignIn.includes(location.pathname)) {
    return (
      <div style={{ minHeight: "100vh", width: "100%", display: "flex" }}>
        {children}
      </div>
    );
  }

  //-----Menu Items
  const menuItems = [
    {
      key: PATH.EVENT,
      label: <div className={styles.nav}>{t("event")}</div>,
      icon: <UserOutlined />,
    },
    {
      key: PATH.DASHBOARD,
      label: <div className={styles.nav}>{t("dashboard")}</div>,
      icon: <DashboardOutlined />,
    },
    {
      key:PATH.PROFILE,
      label: <div className={styles.nav}>Hồ sơ</div>,
      icon: <ProfileOutlined />
    }
  ];

  return (
    <Layout>
      <Header className="header">
        <Link to={PATH.DASHBOARD}>
          <img src={LOGO} alt="logo" className={styles.logo}></img>
        </Link>
        <div className={styles.header}>
          <div>
            <Select
              className={styles.select}
              defaultValue="vn"
              onChange={(value) => i18n.changeLanguage(value)}
            >
              <Option value="vn">
                <b>Tiếng Việt</b>
                <img
                  alt="english"
                  style={{ width: "1.5rem", height: "1rem", marginLeft:"0.5rem" }}
                  src={vietnam}
                />
              </Option>
              <Option value="en">
                <b>English</b>
                <img
                  alt="english"
                  style={{ width: "1.5rem", height: "1rem", marginLeft:"0.5rem" }}
                  src={english}
                />
              </Option>
            </Select>
          </div>
          <div>
            <Space size="small">
              <Badge count={5} className={styles.badge}>
                {user?.userInfo?.notificationEnable == 1 ? (
                  <BellFilled
                    style={{ color: "#195df5" }}
                    className={styles.bell}
                  />
                ) : (
                  <BellFilled
                    style={{ color: "#cf1212" }}
                    className={styles.bell}
                  />
                )}
              </Badge>
            </Space>
          </div>
          <div>
            <Dropdown
              menu={{
                items,
              }}
              placement="bottom"
            >
              <Avatar
                shape="circle"
                className={styles.avatar}
                size={64}
                src="https://www.invert.vn/media/uploads/uploads/2022/12/03191809-2.jpeg"
              />
            </Dropdown>

            <div className={styles.username}>{user?.userInfo?.fullName}</div>
          </div>
        </div>
      </Header>
      <Layout>
        <Sider
          width={280}
          style={{
            background: colorBgContainer,
            overflow: "scroll-y",
          }}
          collapsible
          collapsed={collapsed}
          onCollapse={(value) => setCollapsed(value)}
        >
          <Menu
            mode="inline"
            defaultSelectedKeys={[PATH.DASHBOARD]}
            defaultOpenKeys={[PATH.SEEK]}
            style={{
              height: "100%",
              borderRight: 0,
            }}
            items={menuItems}
            onClick={(e) => onClick(e)}
          />
        </Sider>
        <Layout
          style={{
            padding: "0 24px 24px",
          }}
        >
          <Content
            style={{
              padding: 24,
              margin: 0,
              minHeight: "85vh",
              background: colorBgContainer,
            }}
          >
            {children}
          </Content>
        </Layout>
      </Layout>
    </Layout>
  );
};
export default LayoutWrapper;
