import i18n from "i18next";
import { initReactI18next } from "react-i18next";

i18n.use(initReactI18next).init({
  lng: "vn",
  fallbackLng: "vn",
  supportedLngs: ["vn", "en"],
  resources: {
    en: {
      translation: {
        dashboard: "Dashboard",
        event: "Event",
      },
    },
    vn: {
      translation: {
        dashboard: "Bảng điều khiển",
        event: "Sự kiện",
      },
    },
  },
});

export default i18n;
